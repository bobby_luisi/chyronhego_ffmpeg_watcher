﻿using System;
using System.Configuration;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Security.Permissions;
using System.ServiceProcess;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using watchFF;

namespace ChyronHego_FFMPEG_Watcher
{
    public partial class Transcoding_Service : ServiceBase
    {
        Thread t;
        private FileSystemWatcher watcher = null;
        public Transcoding_Service()
        {
            InitializeComponent();

        }

        [PermissionSet(SecurityAction.Demand, Name = "FullTrust")]
        protected override void OnStart(string[] args3)
        {
            try
            {
                
                DirectoryInfo watchDirCreation = Directory.CreateDirectory(ReadSetting("watch"));
                DirectoryInfo writeDirCreation = Directory.CreateDirectory(ReadSetting("write"));
                System.Console.WriteLine("Dir" + watchDirCreation);
            }
            catch (Exception e)
            {
                LogEvent("Failure to create dir");
            }

            // This is where the watch directory is specified. 
            string[] args = { "Watercher.exe", ReadSetting("watch") };

            // If a directory is not specified, exit program.
            if (args.Length != 2)
            {
                // Display the proper way to call the program.
                Console.WriteLine("Usage: Watcher.exe (directory)");
                return;
            }

            try
            {
                // Create a new FileSystemWatcher and set its properties.
                watcher = new FileSystemWatcher();
                watcher.Path = args[1];
            }
            catch (Exception e)
            {
                LogEvent(e.ToString());
            }

            try
            {
                /* Watch for changes in LastAccess and LastWrite times and
                   the renaming of files or directories. */
                watcher.NotifyFilter = NotifyFilters.LastAccess | NotifyFilters.LastWrite
               | NotifyFilters.FileName | NotifyFilters.DirectoryName;
                // Only watch text files
                watcher.Filter = "*.*";

                // Add event handlers.
                watcher.Changed += (sender, e) => OnChanged(sender, e);
                watcher.Created += (sender, e) => OnChanged(sender, e);
                // watcher.Deleted += (sender, e) => OnChanged(sender, e);
                watcher.Renamed += new RenamedEventHandler(OnRenamed);
                // Begin watching folder.
                watcher.EnableRaisingEvents = true;
            } catch (Exception e)
            {
                // log 
                Console.WriteLine(e.ToString());
            }

            //  Uncomment code for debug mode.  
            //  Console.WriteLine("Press \'q\' to quit the sample.");
            //  while (Console.Read() != 'q') ;


        }

        static void ReadAllSettings()
        {
            try
            {
                var appSettings = ConfigurationManager.AppSettings;

                if (appSettings.Count == 0)
                {
                    Console.WriteLine("empty");
                }
                    //LogEvent("App settings config is empty");
                else
                {
                    foreach (var key in appSettings.AllKeys)
                    {
                        Console.WriteLine("Key {0} Value {1}", key, appSettings[key]);
                    }
                }
            }
            catch (Exception e)
            {
                //LogEvent("Can't read app.config settings");
                Console.WriteLine("Cann't read app config file");
                Console.Write(e.ToString());
            }
        }

        static string ReadSetting(string key)
        {
            string result = "empty";
            try
            {
                var appSettings = ConfigurationManager.AppSettings;
                result = appSettings[key] ?? "Not Found";
                Console.WriteLine(result);
                return result;
            }
            catch (Exception e)
            {
                //LogEvent("Can't read app.config settings");
                Console.WriteLine("Cann't read app config file");
                Console.Write(e.ToString());
            }
            return result;
        }

        protected override void OnStop()
        {
            watcher.EnableRaisingEvents = false;
            watcher.Dispose();

        }

        internal void TestStartupAndStop(String[] args)
        {
            this.OnStart(args);
        }

        // Define the event handlers.
        private static void OnChanged(object source, FileSystemEventArgs e)
        {
            try
            {
                activeWatch activeWatcherHandler2 = new activeWatch(ReadSetting("watch"), ReadSetting("write"), ReadSetting("transcodeString") );
                LogEvent("Instance activeWatch was built successfully");
            }
            catch (Exception err)
            {
                //LogEvent("Failed to create instance of activeWatch");
                Console.WriteLine(err.StackTrace);
            }

        }

        //  Don't carea bout this rn. 
        private static void OnRenamed(object source, RenamedEventArgs e)
        {
            // Specify what is done when a file is renamed.
            Console.WriteLine("File: {0} renamed to {1}", e.OldFullPath, e.FullPath);
        }

        [PermissionSet(SecurityAction.Demand, Name = "FullTrust")]
        private static void LogEvent(string message)
        {
            string eventSource = "ChyronHego_FFMPEG_Watcher";
            DateTime dt = new DateTime();
            dt = System.DateTime.UtcNow;
            message = dt.ToLocalTime() + ": " + message;
            try
            {
                if (!EventLog.SourceExists(eventSource))
                    EventLog.CreateEventSource(eventSource, message);
                EventLog.WriteEntry(eventSource, message);
            }
            catch (Exception e)
            {
                //Console.WriteLine(e.ToString());
            }
        }
    }
}