﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Security.Permissions;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace watchFF
{
    class activeWatch
    {
        //current Directory 
        private String _workingDir = Environment.GetFolderPath(Environment.SpecialFolder.CommonApplicationData) + @"\\Chyronhego\\Transcoding Service\\";
        private String _transcodeString = "";
        // Init components 
        private String _ArgCmd = "";
        // Initialize vars
        private String _dirWatch = "";
        private String _dirMove = "";
        private String _modifyFile = "";
        private String _outputFile = "";
        public String[] dataStructure = { };
        int count = 0;
        private String currentString = "";

        public activeWatch()
        {
            // default ctor
        }

        public activeWatch(String pathToWatch, String pathToWrite, String transcodeString)
        {
            _transcodeString = transcodeString;
            _dirWatch = pathToWatch;
            _dirMove = pathToWrite;
            Console.Write("Common directory: ");
            Console.WriteLine(Environment.GetFolderPath(Environment.SpecialFolder.CommonApplicationData));
            forEachDirItem();
        }

        private void forEachDirItem()
        {
            System.Console.WriteLine("Inside forEach()");

            foreach (String item in countFiles(_dirWatch))
            {
                currentString = item;
                System.Console.WriteLine("Just ran activeScan()");
                contextCreator(_outputFile);
                processCreate(count++, item, "echo testing");
            }
        }

        private String contextPath(String rPath) => rPath.Replace(_dirWatch, ""); 

        private String contextCreator(String outFile)
        {
            try
            {
                String _contextFilePath = outFile.Replace(_dirMove, "");
                String _contextCreate = _contextFilePath.Replace(Path.GetFileName(outFile), "");

                if (Directory.Exists(_contextCreate))
                {
                    return _contextCreate;
                }
                else
                {
                    try
                    {
                        DirectoryInfo _dirContext = Directory.CreateDirectory(_dirMove + _contextCreate);
                        System.Console.WriteLine("Creating: " + _dirMove + _contextCreate);
                    }
                    catch (Exception e)
                    {
                        System.Console.WriteLine("Failed to create.");
                        LogEvent(e.ToString());
                    }
                    return _contextCreate;
                }
            }
            catch (Exception e)
            {
                LogEvent(e.ToString());
                return e.StackTrace;
            }
        }

        public String[] countFiles(String tmp)
        {
            System.Console.WriteLine("Inside countFiles()");
            String[] _searchDir = null;
            try
            {
                System.Console.WriteLine("Inside countFiles() try");
                // This is where we can ONLY permit .mov to avoid processing a .tmp or something that's mid-copy. 
                _searchDir = Directory.GetFiles(tmp, "*.mov", SearchOption.AllDirectories);  // MOV only for now. 
                dataStructure = _searchDir;
                return _searchDir;
            }
            catch (Exception e)
            {
                System.Console.WriteLine("Inside countFiles() catch");
                Console.WriteLine(e.ToString());
                //LogEvent(e.ToString());
            }
            return _searchDir;
        }

        [PermissionSet(SecurityAction.Demand, Name = "FullTrust")]
        private void processCreate(int tmp, string item, string command){

            _modifyFile = _dirWatch + contextPath(item);
            System.Console.WriteLine("_dirWatch: " + _dirWatch + "\ncontextPath(item): " + contextPath(item) + "\n_modifyFile " + _modifyFile);
            _outputFile = _dirMove + contextPath(item);
            contextCreator(_outputFile);

            _ArgCmd = "ffmpeg.exe -i \"" + _modifyFile + "\" -n -vf " + _transcodeString + " -codec qtrle \"" + _outputFile + "\"";

            var processName = _workingDir + "\\ffmpeg\\ffmpegProcess" + tmp + ".bat";
            var processInfo = new ProcessStartInfo("cmd.exe", "/c" + _ArgCmd);
            processInfo.WorkingDirectory = _workingDir + "\\ffmpeg";
            
            processInfo.CreateNoWindow = true;
            processInfo.UseShellExecute = false;
            processInfo.RedirectStandardError = true;
            processInfo.RedirectStandardOutput = true;

            var process = Process.Start(processInfo);

            process.OutputDataReceived += (object sender, DataReceivedEventArgs e) =>
                Console.WriteLine("output>>" + e.Data);
            process.BeginOutputReadLine();

            process.ErrorDataReceived += (object sender, DataReceivedEventArgs e) =>
                Console.WriteLine("error>>" + e.Data);
            process.BeginErrorReadLine();

            process.WaitForExit();
            File.Delete(_modifyFile);
            
            process.Close();
        }


        [PermissionSet(SecurityAction.Demand, Name = "FullTrust")]
        private static void LogEvent(string message)
        {
            string eventSource = "ChyronHego_FFMPEG_Watcher";
            DateTime dt = new DateTime();
            dt = System.DateTime.UtcNow;
            message = dt.ToLocalTime() + ": " + message;
            try
            {
                if (!EventLog.SourceExists(eventSource))
                    EventLog.CreateEventSource(eventSource, message);
                EventLog.WriteEntry(eventSource, message);
            }
            catch
            {
                
            }
        }
    }
}